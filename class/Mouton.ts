// la classe des moutons

class Mouton{                                               // le nom de la classe a.k.a le "type" des moutons
    readonly nom: string;                                   // le nom du mouton. Il peut être lu par les autres
                                                            // objet d'autres classes mais
                                                            // pas modifié (on ne peut pas renommer un mouton)
    private age: number;                                    // l'age du mouton, c'est privé, les autres classe
                                                            // ne peuvent pas savoir ce qu'il y a dedans

    constructor(nom: string, age: number) {                 // permet de créer un nouveau mouton avec 'new'
        this.nom = nom;                                     // par exemple `new Mouton ("kevin", 16) créé
        this.age = age;                                     // un Mouton qui s'appelle Kévin et a 16 ans
    }

    vieillit(){                                             // Quand on appelle cette méthode sur un mouton son age
        this.age++;                                         // augmente de 1 c'est le seul moyen de modifier l'age
    }                                                       // d'un mouton ce qui évite de le faire rajeunir par exemple

    parle(){                                                // permet "d'afficher" le mouton dans la console
        let message: string =                               // comme elle utilise le nom et l'age le résultat sera
            `Béééé je suis ${this.nom} le mouton et j'ai ${this.age} ans`;
        console.log(message);                               // unique pour chaque mouton (à peu près)
        return message;
    }
}

export default Mouton