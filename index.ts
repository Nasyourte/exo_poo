import Mouton from "./class/Mouton"

// Créer Shaun le mouton de 8 ans
let shaun: Mouton = new Mouton("Shaun", 8)
// faire parler Shaun
shaun.parle()
// faire vieillir shaun
shaun.vieillit()
// refaire parler shaun
shaun.parle()
//essayer de faire rajeunir shaun pour qu'il retrouve ses 4 ans
try{
    // avec du code ici
}catch (error){
    console.log("spoiler ce n'est pas possible")
}
/**
 * Créer le type AnimalSexe qui peut avoir comme valeur soit "Male" soit "Female"
 */
/**
 * Modifier la classe Mouton pour lui ajouter un attribut sexe, privé et de type AnimalSexe
 * Ajouter le choix du sexe dans le constructeur du mouton (valeur par défaut "female")
 * Modifier la méthode parle() du mouton pour qu'il dise "le mouton" ou "la brebis" en fonction du sexe
 */
/**
 * Ajouter la méthode copuler(partenaire: Mouton) à la classe mouton
 * Si le partenaire est du sexe opposé au mouton cette méthode doit renvoyer un nouveau mouton d'age 0
 * dont le nom est composer du nom de sa mere concaténé avec le nom de son père et coupé au milieur (à peu près) avec  'us' à la fin si c'est un male
 * son sexe doit être aléatoire (une chance sur 2)
 */
// créer un mouton Bob, 9 ans, male
// faire copuler Bob et Shaun et récupérer l'enfant dans une variable
// faire parler l'agneau récupéré

/**
 * Modifier la méthode copuler(partenaire: Mouton) pour qu'une erreur soit lever si un des deux moutons à moins de 4 ans
 * cf. throw errors
 */
/**
 * créer un tableau troupeau qui contient 50 moutons avec des noms et des ages aléatoires
 * les ages doivent être en 0 et 12
 * 9 moutons sur 10 doivent en fait être des brebis
 */
// création du tableau
let troupeau: Mouton[] = []
    // .... //
// faire bêler tous les moutons du troupeau

/**
 * Les brebis ne peuvent avoir qu'un agneau par an
 * Faire en sorte en modifiant la classe mouton que la méthode copuler()
 * ne fonctionne pas tant qu'un an (age augmenter de 1) n'est pas passé
 */

/**
 * Créer une classe Vache qui fonctionne comme les moutons
 * Puis créer deux vache (un male et une femelle) et leur faire faire plein de petits veaux
 */

/*
        AH MON DIEU C'EST AFFREUX IL Y A PLEIN DE CODE DUPLIQUE ENTRE VACHE ET MOUTON !!!!
 */

/**
 * Créer une classe Animal qui contient tous le code commun entre les Moutons et les Vaches
 * Faire hériter ( class ... extends ....) (rechercher Inheritance sur google...) les classes
 * Mouton et Vache de Animal.
 * Modifier les classes pour n'avoir que ce qui change entre les deux.
 */

/**
 * Créer la classe Poule qui hérite de Animal
 * Créer une classe Oeuf qui hérite aussi de Animal
 * Ajouter une méthode pondre() à la classe Poule qui retourne un nouvel Oeuf d'age 0 quand on l'appelle
 * Ajouter une classe éclore à Oeuf qui retourne une nouvelle Poule.
 */

// créer un poulailler qui contient 10 poules.
// faire pondre des oeufs à toutes les poules et les ajouter dans un tableau nid
// faire éclore tous les oeufs et ajouter les poules qui en sortent au poulailler

/**
 * Créer cette classe
 * on peut ajouter et retirer des animaux.
 * S'il n'y a plus de place il faut renvoyer une erreur
 * Si on la vide, ça retourne un tableau qui contient tous les animaux qu'il y a dedans
 * Si on appelle vieillir ça fait vieillir tous les animaux dedans
 * afficher() affiche la liste des animaux (le type, le nom et l'age)
 */
/*

 ┌───────────────────────────────┐
 │           Etable              │
 ├───────────────────────────────┤
 │ -nbPlaceMax:Number            │
 │ -animaux:Animal[]             │
 ├───────────────────────────────┤
 │ ajouter(animal:Animal)        │
 │ retirer(animal:Animal):Animal │
 │ vider():Animal[]              │
 │ vieillir()                    │
 │ afficher()                    │
 └───────────────────────────────┘

 */
// pour afficher le type (la classe) d'un objet
// mon_objet.constructor.toString().match(/\w+/g)[1]
// ca ne marche plus quand le code est minifié donc ne pas trop s'y fier dans la vrai vie
console.log(shaun.constructor.toString().match(/\w+/g)[1])


// créer une étable pour y mettre toutes les Vaches
// Une autre avec tous les Moutons



/**
 * Créer une classe poulailler qui hérite de Etable
 * La différence c'est que seules des poules peuvent être mises dedans.
 * Ajouter une méthode pondre() qui fait pondre toutes les poules
 * Quand les poules pondent il faut conserver les oeufs dans un attribut
 *    -nid:Oeuf[]
 * Ajouter une méthode AllerChercherLesOeufs(n):Oeuf[] qui retourne un tableau d'Oeuf
 * avec dedans autant d'Oeuf que n
 * Il faut "transférer" des oeufs de nid vers le résultat autant que possible jusqu'à n
 *
 */
// créer un poulailler de 32 places avec 30 poules dedans
// faire pondre les poules
// récolter 20 oeufs
/**
 * Ajouter une méthode éclore() au poulailler qui fait éclore tous les Oeuf du nid
 * Les poules crées doivent être ajoutées au poulailler
 * Penser à supprimer (delete ...) les oeufs qui ont éclos
 * S'il n'y à pas de assez de place, afficher un message pour prévenir "Il Y A TROP DE POULE !!"
 * et retourner les poules surnuméraires dans un tableau
 */
// faire éclore les oeufs du poulailler
// récupérer les poules en trop et les faire caqueter (il doit y en avoir 8 (30 oeufs de base -20 mangés ca fait 10 pourles en plus et il reste 2 places))
